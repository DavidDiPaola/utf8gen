BIN = utf8gen

$(BIN): main.c
	$(CC) $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf $(BIN)

