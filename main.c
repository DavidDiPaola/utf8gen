/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <string.h>

#include <stdint.h>

enum state_t {
	STATE_NONE,
	STATE_INCOMMENT,
	STATE_INLITERAL,
	STATE_INLITERAL_INESCAPE,
	STATE_INHEX,
};

int
main(int argc, char * * argv) {
	if (argc < 2) {
		fprintf(stderr, "syntax: utf8gen <input file>" "\n");
		return 1;
	}
	const char * ARG_PATH = argv[1];

	FILE * input;
	if (strcmp(ARG_PATH, "-") == 0) {
		input = stdin;
	}
	else {
		input = fopen(ARG_PATH, "r");
		if (!input) {
			perror(ARG_PATH);
			return 2;
		}
	}
	FILE * output = stdout;

	enum state_t state = STATE_NONE;
	char * buffer = NULL;
	char c;
	uint32_t hex;
	while (fread(&c, sizeof(c), 1, input) == 1) {
		if (state != STATE_INHEX) {
			hex = 0;
		}

		if (state == STATE_NONE) {
			if (c == '#') {
				state = STATE_INCOMMENT;
				continue;
			}
			else if (c == '"') {
				state = STATE_INLITERAL;
				continue;
			}
			else if (((c >= '0') && (c <= '9')) || ((c >= 'a') && (c <= 'f')) || ((c >= 'A') && (c <= 'F'))) {
				state = STATE_INHEX;
			}
		}
		if (state == STATE_INCOMMENT) {
			if (c == '\n') {
				state = STATE_NONE;
			}

			continue;
		}
		if (state == STATE_INLITERAL) {
			if (c == '\\') {
				state = STATE_INLITERAL_INESCAPE;
				continue;
			}
			if (c == '"') {
				state = STATE_NONE;
				continue;
			}
			size_t amt = fwrite(&c, sizeof(c), 1, output);
			if (amt < 1) {
				perror("output");
				return 3;
			}
		}
		if (state == STATE_INLITERAL_INESCAPE) {
			char esc;
			if (c == 'r') {
				esc = '\r';
			}
			else if (c == 'n') {
				esc = '\n';
			}
			else {
				esc = c;
			}
			size_t amt = fwrite(&esc, sizeof(esc), 1, output);
			if (amt < 1) {
				perror("output");
				return 4;
			}
			state = STATE_INLITERAL;
			continue;
		}
		if (state == STATE_INHEX) {
			uint8_t h;
			if ((c >= '0') && (c <= '9')) {
				h = (c - '0') + 0x0;
				hex = (hex << 4) | h;
			}
			else if ((c >= 'A') && (c <= 'F')) {
				h = (c - 'A') + 0xA;
				hex = (hex << 4) | h;
			}
			else if ((c >= 'a') && (c <= 'f')) {
				h = (c - 'a') + 0xA;
				hex = (hex << 4) | h;
			}
			else {
				state = STATE_NONE;

				const uint8_t * u = NULL;
				size_t u_length = 0;
				if (hex <= 0x7F) {
					const uint8_t u1[] = {
						hex,
					};
					u = u1;
					u_length = sizeof(u1) / sizeof(*u1);
				}
				else if (hex <= 0x7FF) {
					const uint8_t u2[] = {
						(0b110<<5) | ((hex>>6)&0b11111 ),
						(0b10 <<6) | ((hex>>0)&0b111111),
					};
					u = u2;
					u_length = sizeof(u2) / sizeof(*u2);
				}
				else if (hex <= 0xFFFF) {
					const uint8_t u3[] = {
						(0b1110<<4) | ((hex>>12)&0b1111  ),
						(0b10  <<6) | ((hex>> 6)&0b111111),
						(0b10  <<6) | ((hex>> 0)&0b111111),
					};
					u = u3;
					u_length = sizeof(u3) / sizeof(*u3);
				}
				else if (hex <= 0x10FFFF) {
					const uint8_t u4[] = {
						(0b11110<<3) | ((hex>>18)&0b111   ),
						(0b10   <<6) | ((hex>>12)&0b111111),
						(0b10   <<6) | ((hex>> 6)&0b111111),
						(0b10   <<6) | ((hex>> 0)&0b111111),
					};
					u = u4;
					u_length = sizeof(u4) / sizeof(*u4);
				}
				size_t amt = fwrite(u, sizeof(*u), u_length, output);
				if (amt < u_length) {
					perror("output");
					return 5;
				}

				continue;  /* TODO this will ignore the char after a hex */
			}
		}
	}
}

